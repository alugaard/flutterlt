import 'package:flutter/material.dart';
import 'package:flutter_lt/widgets/FirstAnimationButton.dart';
import 'package:flare_splash_screen/flare_splash_screen.dart';
import 'package:flutter_lt/widgets/PageViewWidget.dart' as pages;

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lease Tailored',
      theme: ThemeData(
        primarySwatch: Colors.deepPurple,
      ),
      home: SplashScreen(
        'assets/logoLT.flr',
        MyHomePage(title: 'LizTailrd'),
        startAnimation: 'logoAnimation',
        backgroundColor: Colors.black,
      ),
      routes: <String, WidgetBuilder>{
        "/pageViewWidget": (BuildContext context)=> new pages.PageViewWidget(),
      },
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> with TickerProviderStateMixin{
  AnimationController _controller;

  @override
  void initState() {
    _controller = new AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
  }

  Color backgroundColor = Colors.white70;
  Color foregroundColor = Colors.blue;


  @override
  Widget build(BuildContext context) {

    //Color backgroundColor = Theme.of(context).cardColor;
    //Color foregroundColor = Theme.of(context).accentColor;

    return Scaffold(
      appBar: AppBar(
        title: Center( child: Text(widget.title)),
      ),
      body: Center(
        child: Column(
            children: [
              RichText(
                text: TextSpan(
                  style: TextStyle(height: 5, fontSize: 40, color: Colors.black),
                  text: '¿Qué deseas hacer?'
                ),
              ),
              Icon(Icons.home, size: 100),
              Icon(Icons.search, size: 100)
            ]
        ),
      ),
        floatingActionButton: new FirstAnimationButton().floatingActionButtonMenu(_controller, context)
    );
  }


}

