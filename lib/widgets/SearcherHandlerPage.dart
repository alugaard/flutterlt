import 'package:flutter/material.dart';

class SearcherHandlerPage extends StatelessWidget {
  @override
  Widget build (BuildContext ctxt) {
    return new Scaffold(
      appBar: new AppBar(
          title: Center(child: Text("Buscador"))
      ),
      body: Center(
        child: Column(
            children: [
              RichText(
                  text: TextSpan(
                      style: TextStyle(height: 2, fontSize: 40, color: Colors.black),
                      text: 'Agrega tus filtros de búsqueda'
                  ),
                  textAlign: TextAlign.center
              )
            ]
        ),
      ),
      floatingActionButton: new FloatingActionButton(
          heroTag: null,
          child: new Icon(Icons.toc),
          onPressed: () {}
      ),
    );
  }
}