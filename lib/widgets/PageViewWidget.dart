import 'package:flutter/material.dart';


class PageViewWidget extends StatefulWidget {
  @override
  _PageViewWidgetState createState() => _PageViewWidgetState();
}

class _PageViewWidgetState extends State<PageViewWidget> {

  final PageController _controller = new PageController(initialPage: 0);

  @override
  Widget build (BuildContext ctxt) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text("Propiedad"),
      ),
      body: new PageView(
        controller: _controller,
        scrollDirection: Axis.horizontal,
        children: <Widget>[
          Icon(Icons.home, size: 200),
          Icon(Icons.add, size: 200),
          Icon(Icons.toc, size: 200),
        ],
      )
    );
  }
}