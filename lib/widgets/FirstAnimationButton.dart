import 'package:flutter/material.dart';
import 'package:flutter_lt/widgets/PropertiesHandlerPage.dart';
import 'package:flutter_lt/widgets/SearcherHandlerPage.dart';
import 'dart:math' as math;

class FirstAnimationButton {
  static const List<IconData> icons = const [ Icons.home, Icons.search];
  Color backgroundColor = Colors.white70;
  Color foregroundColor = Colors.blue;

  Widget floatingActionButtonMenu(AnimationController _controller, BuildContext context ) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: new List.generate(icons.length, (int index) {
        Widget child = new Container(
          height: 70.0,
          width: 56.0,
          alignment: FractionalOffset.topCenter,
          child: new ScaleTransition(
            scale: new CurvedAnimation(
              parent: _controller,
              curve: new Interval(
                  0.0,
                  1.0 - index / icons.length / 2.0,
                  curve: Curves.easeOut
              ),
            ),
            child: new FloatingActionButton(
              heroTag: null,
              backgroundColor: backgroundColor,
              mini: true,
              child: new Icon(icons[index], color: foregroundColor),
              onPressed: () {
                icons[index] == Icons.home
                    ? Navigator.push(context, new MaterialPageRoute(
                    builder: (context) => new PropertiesHandlerPage()))
                    : Navigator.push(context, new MaterialPageRoute(
                    builder: (context) => new SearcherHandlerPage()));
              },
            ),
          ),
        );
        return child;
      }).toList()
        ..add(
          new FloatingActionButton(
            heroTag: null,
            child: new AnimatedBuilder(
              animation: _controller,
              builder: (BuildContext context, Widget child) {
                return new Transform(
                  transform: new Matrix4.rotationZ(
                      _controller.value * 0.5 * math.pi),
                  alignment: FractionalOffset.center,
                  child: new Icon(
                      _controller.isDismissed ? Icons.add : Icons.close),
                );
              },
            ),
            onPressed: () {
              if (_controller.isDismissed) {
                _controller.forward();
              } else {
                _controller.reverse();
              }
            },
          ),
        ),
    );
  }
}